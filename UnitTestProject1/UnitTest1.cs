﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    using System.Diagnostics;
    using String_Calculator;

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void BlankString()
        {
            Calculator cs = new Calculator();
           int result = cs.Add("");
            Console.WriteLine("The Result is "+ result);
        }

        [TestMethod]
        public void OneNumber()
        {
            Calculator cs = new Calculator();
            int result = cs.Add("1");
            Console.WriteLine("The Result is " + result);
        }

        [TestMethod]
        public void TwoNumber()
        {
            Calculator cs = new Calculator();
            int result = cs.Add("1,5");
            Console.WriteLine("The Result is " + result);
        }


        [TestMethod]
        public void NextLineNumber()
        {
            Calculator cs = new Calculator();
            int result = cs.Add("1\n2,3");
            Console.WriteLine("The Result is " + result);
        }

        [TestMethod]
        public void DoubleSlash()
        {
            Calculator cs = new Calculator();
            int result = cs.Add("//;\n1;2");
            Console.WriteLine("The Result is " + result);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeNumber))]
        public void NegativeNumber()
        {
            Calculator cs = new Calculator();
            int result = cs.Add("//;\n1;-42,-8,-5");
            Console.WriteLine("The Result is " + result);
            
        }
    }
}
