﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_Calculator
{
    using System.Diagnostics.CodeAnalysis;

    public class Calculator
    {
        public int Add(string numbers)
        {
            List<int> positivesList = new List<int>();
            List<int> negativesList = new List<int>();
            int sum = 0;
            string[] sep = new string[] {",","\n",";"};
            string[] st = numbers.Split(sep, StringSplitOptions.None);
            if (st.Length == 1)
            {
                if (st[0] == "")
                {
                    return 0;
                }
                else
                {
                    return Int32.Parse(st[0]);
                }
            }
            
            foreach (var num in st)
            {
                if ((num == "//") || (num ==""))
                {

                }
                else
                {
                    int newint = Int32.Parse(num);
                    
                    if (newint > 0)
                    {
                        positivesList.Add(newint);
                    }
                    else
                    {
                        negativesList.Add(newint);
                    }
                    
                    
                }
            }
            if (negativesList.Count > 0)
            {
                string result=null;
                StringBuilder sb = new StringBuilder();
                foreach (var negativenumber in negativesList)
                {
                     sb.Append(negativenumber.ToString()+ ",");
                     //sb.Append(",");
                    
                    result = sb.ToString();
                    
                }
                
                throw new NegativeNumber(result);
            }
            else
            {
                foreach (var number in positivesList)
                {
                    sum += number;

                }
            }
            
            return sum;
        }
       
    }

    public class NegativeNumber : Exception
    {
        public NegativeNumber(string message)
        {
            Console.WriteLine("Negative numbers {0} are included ",message);
        }
        
    }
}
